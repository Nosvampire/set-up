#!/bin/bash

function software() {
  # sudo apt-get update -y && sudo apt-get upgrade -y
  sudo apt install -y htop neovim zsh git curl wget python3 make gcc g++ make apt-utils libkrb5-dev librdkafka-dev libterm-readline-gnu-perl libsasl2-dev libssl-dev libghc-zlib-dev

  # sudo nano /etc/apt/sources.list
  exist=$(grep -c 'deb http://security.ubuntu.com/ubuntu bionic-security main' /etc/apt/sources.list)

  if [ $exist == 0 ]; then
    echo '------------------------------------------------------------------------------------------------------'
    echo 'Adding Source'
    echo '------------------------------------------------------------------------------------------------------'
    echo 'deb http://security.ubuntu.com/ubuntu bionic-security main' | sudo tee -a /etc/apt/sources.list
  fi
  sudo apt update && apt-cache policy libssl1.0-dev
  sudo apt-get install -y libssl1.0-dev
}

function ohmyzsh() {
  sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
}

function nvm() {
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
}

function vscodeExtensions() {
  declare -a extensions=(
    # required
    "dbaeumer.vscode-eslint"
    "eamodio.gitlens"
    "esbenp.prettier-vscode"
    "mikestead.dotenv"
    "PKief.material-icon-theme"
    "rvest.vs-code-prettier-eslint"
    # useful
    "christian-kohler.path-intellisense"
    "shardulm94.trailing-spaces"
    "usernamehw.errorlens"
    "wix.vscode-import-cost"
    # misc
    "aaron-bond.better-comments"
    "christian-kohler.npm-intellisense"
    "dracula-theme.theme-dracula"
    "golang.Go"
    "streetsidesoftware.code-spell-checker"
    "wayou.vscode-todo-highlight"
  )

  for i in "${extensions[@]}"; do
    code --install-extension "$i"
  done
}

function settingUpGit() {
  echo '------------------------------------------------------------------------------------------------------'
  echo ''
  echo 'disable sslVerification'
  git config --global http.sslVerify false
  echo 'enable prune remote'
  git config --global fetch.prune true
  echo ''
  echo '------------------------------------------------------------------------------------------------------'
  echo 'configure git user parameters: use "" '
  read -p 'enter name: ' NAME
  git config --global user.name "$NAME"

  read -p 'enter email: ' EMAIL
  git config --global user.email "$EMAIL"
}

function cloneRepos() {
  # Run this function only with citrix connection.
  # insert repos before run
  declare -a repos=(
      ""
      ""
      ""
      ""
  )

  mkdir -p ~/workspace
  cd workspace

  for i in "${repos[@]}"; do
    git clone "$i"
  done

  cd ~
}

while [ true ]; do
  clear && clear
  echo 'Select an option'
  echo '1 - install software & configure git'
  echo '2 - install ohMyZsh'
  echo '3 - install nvm'
  echo '4 - install vscode extensions (must have vscode installed)'
  echo '5 - clone repos web'
  echo ''
  read -p 'Selected: ' OPTION

  case $OPTION in
  1)
    software
    settingUpGit
    read -p 'Press Enter to Finalize step' INPUT
    ;;
  2)
    ohmyzsh
    read -p 'Press Enter to Finalize step' INPUT
    ;;
  3)
    nvm
    read -p 'Press Enter to Finalize step' INPUT
    ;;
  4)
    vscodeExtensions
    read -p 'Press Enter to Finalize step' INPUT
    ;;
  5)
    cloneRepos
    read -p 'Press Enter to Finalize step' INPUT
    ;;
  esac
done
